import java.util.stream.Stream;

/**
 * Account служит для инкапсуляции низкоуровневой задачи работы с данными учетной записи.
 * Содержит необходимые поля и геттеры для удобство дольнейшей работы с этими данными.
 */
public class Account {
    private final String tel;
    private final String pass;

    public String getTel() {
        return tel;
    }

    public String getPass() {
        return pass;
    }

    public Account(String tel, String pass) {
        this.tel = tel;
        this.pass = pass;
    }

    /**
     * Входной массив преобразуется в поток,
     * в котором каждый элемент заменяется его эквивалетном,
     * но уже объектом класса Account
     *
     * @param accounts массив данных учетной записи вида "телефон:пароль"
     * @return массив учетных записей, в виде объекта Account
     */
    public static Account[] parseAccounts(String[] accounts) {
        return Stream.of(accounts)
                .map(account -> {
                    String[] a = account.split(":");
                    return new Account(a[0], a[1]);
                })
                .toArray(Account[]::new);
    }
}
