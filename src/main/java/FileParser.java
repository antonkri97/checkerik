import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FileParser {

    /**
     * Мы получаем поток строк входного файла
     * и преобразуем в другой поток типа String с помощью метода map.
     * Каждый элемент предыдущего потока обрезается до первого пробела,
     * чтобы получить только телефон и пароль в нужном нам формате.
     * <p>
     * Затем уже измененный поток преобразуем в массив строк и возвращаем его.
     * При исключении возвращаем null (необходимо проверять в месте вызова этого метода на null)
     *
     * @param path путь входного файла
     * @return массив элементов вида "телефон:пароль"
     */
    public static String[] getAccounts(String path) {
        try (Stream<String> stream = Files.lines(Paths.get(path))) {
            return stream.map(line -> line.substring(0, line.indexOf(" "))).toArray(String[]::new);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Получаем указатель на будущий выходной файл
     * Открываем входной поток
     * Получаем поток (Stream<String>) от массива с выходной информацией
     * Сортируем его по уменьшению баланса
     * И затем каждый элемент этого потока мы записываем в файл
     *
     * @param path     путь выходного файла
     * @param balances массив с балансом аккаунтов вида "телефон:пароль || баланс: хххх"
     * @throws IOException исключение может возникнуть при работе с файлом
     */
    public static void saveToFile(String path, String[] balances) {
        Path p = Paths.get(path);

        try (BufferedWriter writer = Files.newBufferedWriter(p)) {
            Stream.of(balances)
                    .sorted((b1, b2) -> {
                        int firstBalance = Integer.parseInt(b1.substring(b1.lastIndexOf(' ') + 1));
                        int secondBalance = Integer.parseInt(b2.substring(b2.lastIndexOf(' ') + 1));
                        return Integer.compare(secondBalance, firstBalance);
                    })
                    .forEach(b -> {
                        try {
                            writer.write(b + "\n");
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveToFile(String path, String balance) {
        Path p = Paths.get(path);

        try (BufferedWriter writer = Files.newBufferedWriter(p)) {
            writer.append(balance + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
