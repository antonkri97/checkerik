public class Main {
    /**
     * @param args массив, содержащий путь входного и выходного файла
     */
    public static void main(String[] args) {
        if (args.length != 2) return;

        // получаем массив строк вида "телефон:пароль"
        String[] accounts = FileParser.getAccounts(args[0]);
        if (accounts == null) return;

        Perekrestok perekrestok = new Perekrestok(Account.parseAccounts(accounts));
        String[] balances = perekrestok.getBalances();
        perekrestok.quit();

        // Сохраняем в выходной файл
        FileParser.saveToFile(args[1], balances);
    }
}