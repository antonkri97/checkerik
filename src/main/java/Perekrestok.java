import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Perekrestok {
    private PhantomJSDriver driver; // драйвер для работы с js
    private Account[] accounts; // массив всех аккаунтов
    private Account account;

    /**
     * @param accounts массив учетных записей
     */
    public Perekrestok(Account[] accounts) {
        this.driver = new PhantomJSDriver();
        this.accounts = accounts;
    }

    public Perekrestok(Account account) {
        this.driver = new PhantomJSDriver();
        this.account = account;
    }

    /**
     * Преобразуем массив аккаунтов в поток,
     * который затем преобразуем в строковый массив вида "телефон:пароль || баланс: хххх"
     * функция, которую мы передаем в map() пытается зайти в личный кабинет,
     * в случаем успеха взять баланс и создать результирующую строку
     * и выйти из аккаунта.
     * Если попытки входа или выхода завершились неудачей, баланс будет равен "-1"
     *
     * @return массив строк вида "телефон:пароль || баланс: хххх"
     */
    public String[] getBalances() {
        return Stream.of(accounts)
                .map(account -> {
                    if (!tryLogin(account)) return getBadResult(account);
                    String res = getSuccessResult(account, getBalance());
                    if (!logout()) return getBadResult(account);
                    return res;
                })
                .toArray(String[]::new);
    }

    public String getSingleBalance() {
        if (!tryLogin(account)) return getBadResult(account);
        String res = getSuccessResult(account, getBalance());
        if (!logout()) return getBadResult(account);
        return res;
    }

    /**
     * Пробуем войти 2 раза
     *
     * @param account данные от личного кабинета
     * @return true - в случае успешной авторизации, иначе false
     */
    private boolean tryLogin(Account account) {
        for (int i = 0; i < 2; i++) {
            login(account);
            if (checkUrl()) {
                return true;
            }
        }
        return checkUrl();
    }

    /**
     * Проверяем на какой странице мы находимся на данный момент,
     * так как возможно на момент вторичного логина мы могли уже перейти в личный кабинет
     * <p>
     * Если все же мы не перешли - открываем страницу с авторизацией
     * <p>
     * Нажимаем на опцию логина по телефону
     * Вводим телефон и пароль
     * Нажимаем "войти"
     *
     * @param account данные от личного кабинета
     */
    private void login(Account account) {
        if (checkUrl()) return;
        driver.get("https://my.perekrestok.ru/login");

        try {
            // Опция входа по номеру телефона
            driver.findElement(By.xpath("/html/body/div/div/ng-view/div[2]/div/div[1]/div[2]")).click();

            // телефон
            driver.findElement(By.xpath("/html/body/div/div/ng-view/div[2]/div/div[3]/form/ul/li[1]/div/span/input"))
                    .sendKeys(account.getTel());

            // пароль
            driver.findElement(By.xpath("/html/body/div/div/ng-view/div[2]/div/div[3]/form/ul/li[2]/div/span/input"))
                    .sendKeys(account.getPass());

            // войти
            driver.findElement(By.xpath("/html/body/div/div/ng-view/div[2]/div/div[3]/form/ul/li[3]/div/button")).click();
        } catch (NoSuchElementException ex) {
            return;
        }

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Проверяем, что находимся в личном кабинете
     * и просто выходим из личного кабинета
     * Ожидаем 1 секунду, даём джаваскрипту выполниться
     *
     * @return true, если благополочно вышли, иначе false
     */
    private boolean logout() {
        if (!checkUrl()) return false;

        driver.findElement(By.xpath("/html/body/header/div/div/div/ul/li[4]/a")).click();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return driver.getCurrentUrl().equalsIgnoreCase("https://my.perekrestok.ru/login");
    }

    /**
     * Обращаемся к элементу на главной странице личного кабинета,
     * который содержит баналс
     *
     * @return Баланс
     */
    private String getBalance() {
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String b;
        try {
            b = driver.findElement(By.xpath("/html/body/div/div[2]/ng-view/div/div[1]/div[2]/div/div/ul/li[2]/a")).getText();
        } catch (NoSuchElementException e) {
            b = "0";
        }

        return b.isEmpty() ? "0" : b;
    }

    /**
     * Мы получаем поток с нашего массива валидных url'ов
     * Мы перешли в личный кабинет если,
     * хотя бы одна строка с этого потока равна текущему url
     *
     * @return true, если мы успешно перешли в личный кабинет, иначе false
     */
    private boolean checkUrl() {
        String[] validURL = {
                "https://my.perekrestok.ru/profile#profile_details",
                "https://my.perekrestok.ru/profile",
                "https://my.perekrestok.ru/#profile_details",
                "https://my.perekrestok.ru/"
        };
        return Stream.of(validURL).anyMatch(url -> url.equalsIgnoreCase(driver.getCurrentUrl()));
    }

    /**
     * Этот метод следует вызывать в случае обнаружении баланса
     *
     * @param account данные личного кабинета
     * @param balance баланс данного личного кабинета
     * @return строка с телефоном, паролем и балансов данного личного кабинета
     */
    private String getSuccessResult(Account account, String balance) {
        return String.format("%s:%s || баланс: %s", account.getTel(), account.getPass(), balance);
    }

    /**
     * Этот метод следует вызывать в случае неудачи,
     * когда баланс, по каким-то причинам, обнаружить не удалось
     *
     * @param account данные личного кабинета
     * @return строка с телефоном, паролем и балансов равным "-1"
     */
    private String getBadResult(Account account) {
        return String.format("%s:%s || баланс: -1", account.getTel(), account.getPass());
    }

    /**
     * Завершаем работу драйвера
     */
    public void quit() {
        driver.quit();
    }
}
